# Haruka Edu Test

Berikut tahapan untuk install program:
- execute : git clone git@gitlab.com:farhanoeoeoe/farhan_harukaedu.git
- setelah selesai, masuk ke directory farhan_harukaedu
- execute : composer install
- buat file .env di root project
- copy isi file .env.example ke dalam file .env
- execute : php artisan key:generate
- execute : php artisan serve
- buka url di browser
