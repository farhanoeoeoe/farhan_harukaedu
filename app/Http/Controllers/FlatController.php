<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class FlatController extends Controller {

    public function index() {
        $array = array(
            array(
                1,
                2,
                array(3)
            ),
            4
        );

        $array2 = array(
            array(
                array(1,2,3),
                4,
                array(5)
            ),
            6,
            array(7),
            8,
            array(9, array(10))
        );

        $flat_array  = array();
        $flat_array2 = array();

        $result[0]['source'] = $array;
        $result[0]['result'] = $this->getDataFromArray($array, $flat_array);

        $result[1]['source'] = $array2;
        $result[1]['result'] = $this->getDataFromArray($array2, $flat_array2);

        $data['number'] = 1;
        $data['data'] = $result;

        return view('flat', $data);
    }

    private function getDataFromArray($data_array, $flat_array) {
        foreach ($data_array as $data) {
            if (is_array($data)) {
                $flat_array = $this->getDataFromArray($data, $flat_array);
            } else {
                $flat_array[] = $data;
            }
        }

        return $flat_array;
    }

}
