<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class DistanceController extends Controller {

    public function index() {
        $students = Excel::load(public_path() . '/file/students.csv')->toArray();

        $student_result = array();
        $r = 6371; // Earth Radius (Km)
        $i = 0;

        foreach ($students as $student) {
            // Student Coordinate
            $lat1  = deg2rad($student['latitude']);
            $long1 = deg2rad($student['longitude']);

            // Gandaria City Coordinate
            $lat2  = deg2rad(-6.243376);
            $long2 = deg2rad(106.784425);

            $distance = $this->getDistance($r, $lat1, $long1, $lat2, $long2);

            if ($distance <= 14.00) {
                $student_result[$i]['id'] = $student['id'];
                $student_result[$i]['name'] = $student['name'];
                $i++;
            }
        }

        // Sort by ID
        foreach ($student_result as $key => $value) {
            $id[$key] = $value['id'];
        }

        array_multisort($id, SORT_ASC, $student_result);

        $data['number'] = 1;
        $data['data']   = $student_result;

        return view('distance', $data);
    }

    private function getDistance($r, $lat1, $long1, $lat2, $long2) {
        $distance = $r * acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos(abs($long1 - $long2)));

        return round($distance, 2);
    }

}
