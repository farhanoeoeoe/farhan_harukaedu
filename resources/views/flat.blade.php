<!DOCTYPE html>
<html>
    <head>
        <title>Haruka Edu Test - Flat</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 32px;
                margin-bottom: 10px;
            }

            .body {
                font-size: 16px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    Flat Array
                </div>

                <div class="body">
                    <table border="1">
                        <tr>
                            <th>Example</th>
                            <th>Source</th>
                            <th>Result</th>
                        </tr>
                        @foreach ($data as $value)
                        <tr>
                            <td>{{ $number++ }}</td>
                            <td><?php printf("<pre>%s</pre>", print_r(json_encode($value['source']), TRUE)) ?> </td>
                            <td><?php printf("<pre>%s</pre>", print_r(json_encode($value['result']), TRUE)) ?> </td>
                        </tr>
                        @endforeach
                    </table>
                    <br>
                    <a href="{{ URL::to('') }}"><button type="button">Home</button></a>
                </div>
            </div>
        </div>
    </body>
</html>
