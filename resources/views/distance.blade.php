<!DOCTYPE html>
<html>
    <head>
        <title>Haruka Edu Test - Distance</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 32px;
                margin-bottom: 10px;
            }

            .body {
                font-size: 16px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    Distance
                </div>

                <div class="body">
                    <table border="1" width="500px">
                        <tr>
                            <th>Number</th>
                            <th>ID</th>
                            <th>Name</th>
                        </tr>
                        @foreach ($data as $value)
                        <tr>
                            <td>{{ $number++ }}</td>
                            <td>{{ $value['id'] }}</td>
                            <td>{{ $value['name'] }}</td>
                        </tr>
                        @endforeach
                    </table>
                    <br>
                    <a href="{{ URL::to('') }}"><button type="button">Home</button></a>
                </div>
            </div>
        </div>
    </body>
</html>
