<!DOCTYPE html>
<html>
    <head>
        <title>Haruka Edu Test</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 32px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    <H3>HARUKA EDU TEST</H3>
                    <a href="flat">Flat</a>
                    <br>
                    <a href="distance">Distance</a>
                </div>
            </div>
        </div>
    </body>
</html>
